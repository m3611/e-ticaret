﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace UrunKatalog.Web.Entity
{
    public class DataContext : DbContext
    {
        public DataContext() : base("dataConnection")
        {
            //Database.SetInitializer(new DataInitializer());

            //if (!Database.Exists())
            //{
            //    Database.Initialize(true);
            //}
        }

        public DbSet<Product> Products    { get; set; }
        public DbSet<Category> Categories { get; set; }
    }
}