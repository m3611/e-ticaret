﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;

namespace UrunKatalog.Web.Entity
{
    public class DataInitializer : DropCreateDatabaseIfModelChanges<DataContext> /*DropCreateDatabaseAlways<DataContext>*/
    {
        protected override void Seed(DataContext context)
        {
            List<Category> kategoriler = new List<Category>()
            {
                new Category(){Name="Etek"     ,Description="Etek Urunleri"},
                new Category(){Name="Pantolon" ,Description="Pantolon Urunleri"},
                new Category(){Name="Eşofman"  ,Description="Eşofman Urunleri"},
                new Category(){Name="Bluz"     ,Description="Bluz Urunleri"},
                new Category(){Name="Elbise"   ,Description="Elbise Urunleri"},
            };

            foreach (var item in kategoriler)
            {
                context.Categories.Add(item);
            }

            context.SaveChanges();

            var urunler = new List<Product>()
            {
                new Product(){Name="Etek",Description="Cok guzel bir etektir",Price=150,Stock=10,IsApproved=true,CategoryId=1,IsHome=true,Image="etek.jpg"},
                new Product(){Name="Etek",Description="Cok guzel bir etektir",Price=150,Stock=10,IsApproved=true,CategoryId=1,IsHome=true,Image="etek.jpg"},
                new Product(){Name="Etek",Description="Cok guzel bir etektir",Price=150,Stock=10,IsApproved=true,CategoryId=1,IsHome=true,Image="etek.jpg"},
                new Product(){Name="Etek",Description="Cok guzel bir etektir",Price=150,Stock=10,IsApproved=true,CategoryId=1,Image="etek"},
                new Product(){Name="Etek",Description="Cok guzel bir etektir",Price=150,Stock=10,IsApproved=true,CategoryId=1,Image="etek"},

                new Product(){Name="Pantolon",Description="Aciklama Ekleyebilirsin",Price=210,Stock=10,IsApproved=true,CategoryId=2,IsHome=true,Image="pantolon1.jpg"},
                new Product(){Name="Pantolon",Description="Aciklama Ekleyebilirsin",Price=200,Stock=10,IsApproved=true,CategoryId=2,IsHome=true,Image="pantolon2.jpg"},
                new Product(){Name="Pantolon",Description="Aciklama Ekleyebilirsin",Price=195,Stock=10,IsApproved=true,CategoryId=2,Image="pantolon3.jpg"},
                new Product(){Name="Pantolon",Description="Aciklama Ekleyebilirsin",Price=195,Stock=10,IsApproved=true,CategoryId=2,Image="pantolon3.jpg"},
                new Product(){Name="Pantolon",Description="Aciklama Ekleyebilirsin",Price=195,Stock=10,IsApproved=true,CategoryId=2,Image="pantolon3.jpg"},

                new Product(){Name="Eşofman",Description="Aciklama Ekleyebilirsin"  ,Price=150,Stock=10,IsApproved=true,CategoryId=3,IsHome=true,Image="esofman.jpg"},
                new Product(){Name="Eşofman" ,Description="Aciklama Ekleyebilirsin" ,Price=145,Stock=10,IsApproved=true,CategoryId=3,IsHome=true,Image="esofman.jpg"},
                new Product(){Name="Eşofman" ,Description="Aciklama Ekleyebilirsin" ,Price=150,Stock=10,IsApproved=true,CategoryId=3,Image="esofman2.jpg"},
                new Product(){Name="Eşofman" ,Description="Aciklama Ekleyebilirsin" ,Price=150,Stock=10,IsApproved=true,CategoryId=3,Image="esofman3.jpg"},
                new Product(){Name="Eşofman" ,Description="Aciklama Ekleyebilirsin" ,Price=150,Stock=10,IsApproved=true,CategoryId=3,Image="esofman3.jpg"},

                new Product(){Name="Bluz",Description="Aciklama Ekleyebilirsin",Price=160,Stock=10,IsApproved=true,CategoryId=4,IsHome=true},
                new Product(){Name="Bluz",Description="Aciklama Ekleyebilirsin",Price=160,Stock=10,IsApproved=true,CategoryId=4,IsHome=true},
                new Product(){Name="Bluz",Description="Aciklama Ekleyebilirsin",Price=160,Stock=10,IsApproved=true,CategoryId=4},
                new Product(){Name="Bluz",Description="Aciklama Ekleyebilirsin",Price=160,Stock=10,IsApproved=true,CategoryId=4},
            };

            foreach (var item in urunler)
            {
                context.Products.Add(item);
            }

            context.SaveChanges();

            base.Seed(context);
        }
    }
}