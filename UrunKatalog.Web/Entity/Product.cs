﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace UrunKatalog.Web.Entity
{
    public class Product
    {
        public int Id               { get; set; }
        [DisplayName("Ürün Adı")]
        public string Name          { get; set; }
        [DisplayName("Ürün Açıklaması")]
        public string Description   { get; set; }
        [DisplayName("Resim")]
        public string Image         { get; set; }
        [DisplayName("Fiyat")]
        public double Price         { get; set; }
        [DisplayName("Stok Sayısı")]
        public int Stock            { get; set; }
        [DisplayName("AnaSayfa Ürünü")]
        public bool IsHome          { get; set; }
        [DisplayName("Onaylı Mı ?")]
        public bool IsApproved      { get; set; }
        public int CategoryId       { get; set; }
        public Category Category    { get; set; }
    }
}